<?php

/**
 * White Paper Section Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$context['block'] = $block;
$context['papers'] = get_field('acf_wps_papers');

if(!session_id()) {
  session_start();
}

if(isset($_GET['unbounce'])){
  $context['unbounce'] = true;
}

Timber::render('templates/blocks/white-paper-section.twig',  $context);
?>