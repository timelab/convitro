<?php

/**
 * Featured Section Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$context['block'] = $block;
$context['banner_background'] = get_field('acf_cb_background');
$context['content'] = get_field('acf_cb_content');
$context['content_phone'] = get_field('acf_cb_content_phone');

Timber::render('templates/blocks/contact-banner.twig',  $context);
?>