<?php

/**
 * Switchable Image Section Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$context['block'] = $block;
$sis_image = get_field('acf_sis_image');
//$context['sis_image'] = $sis_image['sizes']['switchable-image']; //Blev en visuell bugg med exakt storlek
$context['sis_image'] = $sis_image['url'];
$context['sis_position'] = get_field('acf_sis_position');
$context['sis_text'] = get_field('acf_sis_text');
$context['sis_link'] = get_field('acf_sis_link');
$context['sis_link_text'] = get_field('acf_sis_link_text');
$context['sis_link_url'] = get_field('acf_sis_link_url');

Timber::render('templates/blocks/switchable-image-section.twig',  $context);
?>