<?php

/**
 * Featured Section Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$context['block'] = $block;
$context['section_title'] = get_field('acf_fc_title');
$context['main_column'] = get_field('acf_fb_main_column');
$context['feature_boxes'] = get_field('acf_fb_feature_boxes');
$context['link_group'] = get_field('acf_fb_link_group');
$context['padding_bottom'] = get_field('acf_fb_padding_bottom');

Timber::render('templates/blocks/featured-section.twig',  $context);
?>