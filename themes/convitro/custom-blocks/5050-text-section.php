<?php

/**
 * Featured Section Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$context['block'] = $block;
$context["text_columns"] = get_field("acf_5050ts_columns");

Timber::render('templates/blocks/5050-text-section.twig',  $context);
?>