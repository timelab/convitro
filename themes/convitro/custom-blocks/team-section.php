<?php

/**
 * Team Section Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$context = Timber::context();
$context['block'] = $block;
$context['section_title'] = get_field('acf_tts_section_title');
$context['the_team'] = get_field('acf_tts_team');

Timber::render('templates/blocks/team-section.twig',  $context);
?>