<?php

/**
 * Text Section Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$context['block'] = $block;
$context['group_1'] = get_field('acf_rs_group_1');
$context['group_2'] = get_field('acf_rs_group_2');

Timber::render('templates/blocks/reference-section.twig',  $context);
?>