<?php

/**
 * Hero Section Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$context['block'] = $block;
$context['flex_class'] = "";
$context['hs_image'] = get_field('acf_hs_background_image');
$context['hs_bg_color'] = get_field('acf_hss_background_color');
$context['parallax'] = get_field('acf_hss_parallax');
$context['p_speed'] = get_field('acf_hs_parallax_speed');
$context['hs_text'] = get_field('acf_hs_text');
$context['hss_button'] = get_field('acf_hs_button');
$context['hss_button_text'] = get_field('acf_hs_button_text');
$context['hss_button_link'] = get_field('acf_hs_button_link');

switch(get_field('acf_hs_hz_text_alignment')){
  case "text-left":
    $context['flex_class'] .= "justify-content-start";
    break;
  case "text-center":
    $context['flex_class'] .= "justify-content-center";
    break;
  case "text-right":
    $context["flex_class"] = "justify-content-end";
    break;
}

Timber::render('templates/blocks/hero-section-split.twig',  $context);
?>