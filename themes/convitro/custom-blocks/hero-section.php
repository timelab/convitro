<?php

/**
 * Hero Section Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$context['block'] = $block;
$context['hs_image'] = get_field('acf_hs_background_image');
$context['p_speed'] = get_field('acf_hs_parallax_speed');
$context['flex_class'] = "";
$context['hs_text'] = get_field('acf_hs_text');
$context['hs_text_size'] = get_field('acf_hs_text_size');
$context['hs_text_color'] = get_field('acf_hs_text_farg');

switch(get_field('acf_hs_hz_text_alignment')){
  case "text-center":
    $context['flex_class'] .= "justify-content-center";
    break;
  case "text-right":
    $context["flex_class"] = "justify-content-end";
    break;
}

switch (get_field('acf_hs_vt_text_alignment')) {
  case 'text-top':
    $context['flex_class'] .= " align-items-start";
    break;
  case 'text-bottom':
    $context['flex_class'] .= " align-items-end";
    break;
  default:
    $context['flex_class'] .= " align-items-center";
    break;
}

Timber::render('templates/blocks/hero-section.twig',  $context);
?>