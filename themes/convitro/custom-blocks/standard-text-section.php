<?php

/**
 * Text Section Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$context['block'] = $block;
$context['content'] = get_field('acf_sts_content');
$context['background_color'] = get_field('acf_sts_background_color');

Timber::render('templates/blocks/standard-text-section.twig',  $context);
?>