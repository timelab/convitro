<?php

/**
 * Video Section Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$context['block'] = $block;
$context['vs_title'] = get_field('acf_vs_title');
$context['vs_video_url'] = get_field('acf_vs_video_url');

$video_image = get_field('acf_vs_video_image');
$context['vs_video_image'] = $video_image['sizes']['video_thumbnail'];

Timber::render('templates/blocks/video-section.twig',  $context);
?>