<?php

/**
 * Text Section Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$context['block'] = $block;
$context['tss_text'] = get_field('acf_tss_text');
$context['tss_link'] = get_field('acf_tss_link');
$context['bg_color'] = get_field('acf_tss_background_color');
$context['tss_class'] = "";


if($context['tss_link']){
  $context['tss_link'] = $context['tss_link'][0];
}

if(get_field('acf_tss_extra_padding')) {
  $context['tss_class'] = "extra-padding";
}

Timber::render('templates/blocks/text-section-styled.twig',  $context);
?>