<?php

/**
 * Tabbed view Section Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$context['block'] = $block;
$context['tv_tabs'] = get_field('acf_tv_tabs');

Timber::render('templates/blocks/tabbed-view-section.twig',  $context);
?>