<?php
  function get_posts_by_type($post_type, $posts_per_page = -1, $offset = 0){
    $args = array(
      'post_type'      => $post_type,
      'orderby'        => 'menu_order',
      'order'          => 'ASC',
      'posts_per_page' => $posts_per_page,
      'offset'         => $offset,
    );

    $posts_array = get_posts( $args );
    foreach ($posts_array as $key => $single) {
      $posts_array[$key]->acf = get_fields($single->ID);
    }

    return $posts_array;
  }

?>