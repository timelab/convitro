<?php
  /**
   * Format for making a custom post type
   * makeCustomPostType('singular', 'plural', 'icon', has single)
   * singluar = lowecase post type name in singular
   * plural = same as singular but in plural form
   * icon = code for icon can be src to an image or dashicon code
   * Dash icons = https://developer.wordpress.org/resource/dashicons/
   * has single = bool if the post type has single pages
   */

  function makeCustomPostType($singular, $plural, $icon = null, $has_single = true){
    $capital_plural = ucfirst ( $plural );
    $capital_singular = ucfirst ( $singular );
    $formated = str_replace(" ", "-", $singular);
    $text_domain = get_bloginfo( 'name' );
    $icon = ($icon === null) ? 'dashicons-admin-generic' : $icon;

    $labels = array(
      'name'               => _x( $capital_plural, 'post type general name', $text_domain ),
      'singular_name'      => _x( $capital_singular, 'post type singular name', $text_domain ),
      'menu_name'          => _x( $capital_plural, 'admin menu', $text_domain ),
      'name_admin_bar'     => _x( $capital_singular, 'add new on admin bar', $text_domain ),
      'add_new'            => _x( 'Add New', $singular, $text_domain ),
      'add_new_item'       => __( 'Add New ' . $capital_singular, $text_domain ),
      'new_item'           => __( 'New ' . $capital_singular, $text_domain ),
      'edit_item'          => __( 'Edit ' . $capital_singular, $text_domain ),
      'view_item'          => __( 'View ' . $capital_singular, $text_domain ),
      'all_items'          => __( 'All ' . $capital_plural, $text_domain ),
      'search_items'       => __( 'Search ' . $capital_plural, $text_domain ),
      'parent_item_colon'  => __( 'Parent ' . $capital_plural . ':', $text_domain ),
      'not_found'          => __( 'No ' . $plural . ' found.', $text_domain ),
      'not_found_in_trash' => __( 'No ' . $plural . ' found in Trash.', $text_domain )
    );


    $args = array(
      'labels'             => $labels,
      'description'        => __( 'Description.', $text_domain ),
      'menu_icon'          => $icon,
      'public'             => true,
      'publicly_queryable' => true,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'rewrite'            => array( 'slug' => $formated ),
      'capability_type'    => 'post',
      'has_archive'        => true,
      'hierarchical'       => false,
      'menu_position'      => null,
      'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
    );

    if($has_single == false){ $args['publicly_queryable'] = false; }

    register_post_type( $formated, $args );
  }

?>