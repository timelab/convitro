const gulp = require('gulp')
const { src, dest, parallel } = gulp;
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const cleanCss = require('gulp-clean-css');
const autoPrefix = require('autoprefixer');
const path = require('path');
const rename = require('gulp-rename');
const postcss = require('gulp-postcss');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');

function buildJs() {
  return src([
    './assets/js/vendor/*.js',
    './node_modules/bootstrap/dist/js/bootstrap.bundle.js',
    './node_modules/aos/dist/aos.js',
    './assets/js/_app.js',
    './assets/js/parallax.js',
    './assets/js/all_scripts.js'
  ])
  .pipe(concat('main.min.js'))
  .pipe(uglify())
  .pipe(dest('./assets/js/'));
}

function buildAdminJs() {
  return src(['./node_modules/aos/dist/aos.js','./assets/js/admin.js','./assets/js/all_scripts.js'])
  .pipe(concat('blocks.js'))
  .pipe(dest('./assets/js/'));
}

function buildCss() {
  return src('./assets/scss/app.scss')
  .pipe(sass({ outputStyle: 'expanded', includePaths: ['node_modules'] }))
  .pipe(postcss([ autoPrefix({ cascade: false }) ]))
  .pipe(cleanCss())
  .pipe(rename('main.min.css'))
  .pipe(dest("./assets/css/"));
}

function buildBackendCss() {
  return src('./assets/scss/backend.scss')
  .pipe(sass({ outputStyle: 'expanded', includePaths: ['node_modules'] }))
  .pipe(postcss([ autoPrefix({ cascade: false }) ]))
  .pipe(cleanCss())
  .pipe(rename('backend.min.css'))
  .pipe(dest("./assets/css/"));
}

function watchFiles() {
  // Watch scss
  gulp.watch('./assets/scss/**/*.scss', parallel(buildCss, buildBackendCss));

  // Watch JS
  gulp.watch(['./assets/js/**/_*.js', './assets/js/all_scripts.js', './assets/js/admin.js'], parallel(buildJs, buildAdminJs));
}


exports.watch = watchFiles;
exports.css = parallel(buildCss, buildBackendCss);
exports.js = parallel(buildJs, buildAdminJs);
exports.default = parallel(buildJs, buildAdminJs, buildCss, buildBackendCss);