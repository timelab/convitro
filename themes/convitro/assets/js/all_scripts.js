// Overlay functions and listeners
var overlay = false;
var privacy = false;
var thank = false;

// Global function to be referenced in cookie settings file.
function enablePrivacy() {
  if(privacy) {
    privacy = false;
    if($(".contact-overlay").css("display") === "none") {
      $("body").css("overflow", "auto");
    }
    $(".contact-privacy").css("display", "none");
  } else {
    privacy = true;
    $("body").css("overflow", "hidden");
    $(".contact-privacy").css("display", "initial");
  }
}


function init_all() {
  // Init all js for all pages including admin blocks

  // Animate on scroll
  if (typeof AOS != 'undefined') {
    AOS.init({
      offset: 120, // offset (in px) from the original trigger point
      delay: 0, // values from 0 to 3000, with step 50ms
      duration: 400, // values from 0 to 3000, with step 50ms
      easing: 'ease-out', // default easing for AOS animations
      once: true,
    });
  }

  var is_public = $("body").data('template');

  //$('.contact-banner').parallax();

  $('.toggle-container .burger').click(function(e){
    if($('.toggle-container').hasClass('active')) {
      $('body').removeClass('no-scroll');
      $('.toggle-container').removeClass('active');
      $('#menu1').removeClass('active').fadeOut();
    } else {
      $('.toggle-container').addClass('active');
      $('body').addClass('no-scroll');
      $('#menu1').fadeIn(function(){
        $('#menu1').addClass('active');
      });
    }
  });

  $(".toggle-overlay").click(function(e) {
    toggleOverlay();
  });

  $(".toggle-privacy").click(function(e) {
    togglePrivacy();
  });

  $(".close-privacy").click(function(e) {
    $("body").css("overflow", "auto");
  });

  $(".toggle-thank").click(function(e) {
    toggleThank();
  });

  $(document).on("wpcf7submit", function(e) {
    toggleThank();
  });

  function toggleOverlay() {
    if(overlay) {
      overlay = false;
      $("body").css("overflow", "auto");
      $(".contact-overlay").css("display", "none");
    } else {
      overlay = true;
      $("body").css("overflow", "hidden");
      $(".contact-overlay").css("display", "initial");
    }
  }

function togglePrivacy() {
    if(privacy) {
      privacy = false;
      $(".contact-privacy").css("display", "none");
    } else {
      privacy = true;
      $("body").css("overflow", "hidden");
      $(".contact-privacy").css("display", "initial");
    }
  }

  function toggleThank() {
    if(thank) {
      thank = false;
      $(".contact-thank").css("display", "none");
    } else {
      thank = true;
      $("body").css("overflow", "hidden");
      $(".contact-thank").css("display", "initial");
    }
  }

  function show_hide_menu_item(li, action, start) {
    window.setTimeout(function(){
      if(action == 'hide') {
        $(li).fadeOut(100);
      } else {
        $(li).fadeIn(300);
      }
    },start);
  }

  if(is_public){
    // Enable parallax for devices over 991 innerwidth
    if(window.innerWidth > 991) {
      $('.hero-section').parallax();
      $('.split-image').parallax();
      $('.contact-banner').parallax();
    }
  } else {
    // In admin, add as background
    $('.hero-section').each(function(){
      $(this).css({'background-image' : 'url('+$(this).data('image-src')+')'});
    });
    $('.split-image').each(function(){
      $(this).css({'background-image' : 'url('+$(this).data('image-src')+')'});
    });
  }

  // Animated tabbed view variable
  if($(".nav-tabs").length >= 1) {
    var offset = $(".nav-tabs").offset().top;
  }


  $(window).scroll(function() {
    if(window.scrollY > 0 && !$('.nav-wrapper').hasClass('white')) {
      $('.nav-wrapper').addClass('white');
    }

    if(window.scrollY == 0 && $('.nav-wrapper').hasClass('white')){
      $('.nav-wrapper').removeClass('white');
    }

    // Animated tabbed view
    if($(".nav-tabs").length >= 1) {
      if ( ($(window).innerHeight() + window.scrollY) > offset ) {
        if(!$(".nav-tabs").hasClass("visible-anim")) {
          $(".nav-tabs").addClass("visible-anim");
          $(".stroke").addClass("visible-anim");
        }
      }
    }
  });

  // Map
  $('.map-holder').on('click', function() {
    $(this).addClass('interact');
  }).removeClass('interact');

  var mapsOnPage = $('.map-container[data-maps-api-key]');
  if(mapsOnPage.length){
    mapsOnPage.addClass('gmaps-active');
    init_api($);
    init_map();
  }


  // Init video section
  var videoCover = $(this);
  if(videoCover.find('iframe[src]').length){
    videoCover.find('iframe').attr('data-src', videoCover.find('iframe').attr('src'));
    videoCover.find('iframe').attr('src','');
  }

  $('.video-cover .video-play-icon').off("click"); // Remove previous click event
  $('.video-cover .video-play-icon').on("click", function(){
    var playIcon = $(this);
    var videoCover = playIcon.closest('.video-cover');
    if(videoCover.find('video').length){
      var video = videoCover.find('video').get(0);
      videoCover.addClass('reveal-video');
      video.play();
      return false;
    }else if(videoCover.find('iframe').length){
      var iframe = videoCover.find('iframe');
      iframe.attr('src',iframe.attr('data-src'));
      videoCover.addClass('reveal-video');
      return false;
    }
  });
$('.toggle-container .burger')
  $('[data-toggle-class]').each(function(){
    var element = $(this),
      data    = element.attr('data-toggle-class').split("|");

    $(data).each(function(){
      var candidate     = element,
        dataArray     = [],
        toggleClass   = '',
        toggleElement = '',
        dataArray = this.split(";");

      if(dataArray.length === 2){
        toggleElement = dataArray[0];
        toggleClass   = dataArray[1];
        $(candidate).on('click',function(){
          if(!candidate.hasClass('toggled-class')){
            candidate.toggleClass('toggled-class');
          }else{
            candidate.removeClass('toggled-class');
          }
          $(toggleElement).toggleClass(toggleClass);
          return false;
        });
      }else{
        console.log('Error in [data-toggle-class] attribute. This attribute accepts an element, or comma separated elements terminated witha ";" followed by a class name to toggle');
      }
    });
  });
}

var mr = {
  'maps' : {
    'options' : {}
  }
}

function init_map(){
  if(typeof window.google !== "undefined"){
    if(typeof window.google.maps !== "undefined"){

      mr.maps.instances = [];


      jQuery('.gmaps-active').each(function(){
        var mapElement      = this,
          mapInstance     = jQuery(this),
          isDraggable     = jQuery(document).width() > 766 ? true : false,
          showZoomControl = typeof mapInstance.attr('data-zoom-controls') !== typeof undefined ? true : false,
          zoomControlPos  = typeof mapInstance.attr('data-zoom-controls') !== typeof undefined ? mapInstance.attr('data-zoom-controls'): false,
          latlong         = typeof mapInstance.attr('data-latlong') !== typeof undefined ? mapInstance.attr('data-latlong') : false,
          latitude        = latlong ? 1 *latlong.substr(0, latlong.indexOf(',')) : false,
          longitude       = latlong ? 1 * latlong.substr(latlong.indexOf(",") + 1) : false,
          geocoder        = new google.maps.Geocoder(),
          address         = typeof mapInstance.attr('data-address') !== typeof undefined ? mapInstance.attr('data-address').split(';'): [""],
          map, marker, markerDefaults,mapDefaults,mapOptions, markerOptions, mapAo = {}, markerAo = {}, mapCreatedEvent;

        mapCreatedEvent    = document.createEvent('Event');
        mapCreatedEvent.initEvent('mapCreated.maps.mr', true, true);




        mapDefaults = {
          disableDefaultUI: true,
          draggable: isDraggable,
          scrollwheel: false,
          styles: [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}],
          zoom: 17,
          zoomControl: false,
        };

        // Attribute overrides - allows data attributes on the map to override global options
        mapAo.styles             = typeof mapInstance.attr('data-map-style') !== typeof undefined ? JSON.parse(mapInstance.attr('data-map-style')): undefined;
        mapAo.zoom               = mapInstance.attr('data-map-zoom') ? parseInt(mapInstance.attr('data-map-zoom'),10) : undefined;
        mapAo.zoomControlOptions = zoomControlPos !== false ? {position: google.maps.ControlPosition[zoomControlPos]} : undefined;

        markerDefaults = {
          icon: {url:( typeof mr_variant !== typeof undefined ? '../': '' )+'/wp-content/themes/arctoslabs/assets/images/mapmarker.png', scaledSize: new google.maps.Size(50,50)},
          title: 'We Are Here',
          optimised: false
        };

        markerAo.icon = typeof mapInstance.attr('data-marker-image') !== typeof undefined ? {url: mapInstance.attr('data-marker-image'), scaledSize: new google.maps.Size(50,50)} : undefined;
        markerAo.title = mapInstance.attr('data-marker-title');

        mapOptions = jQuery.extend({}, mapDefaults, mr.maps.options.map, mapAo);
        markerOptions = jQuery.extend({}, markerDefaults, mr.maps.options.marker, markerAo);

        if(address !== undefined && address[0] !== ""){
          geocoder.geocode( { 'address': address[0].replace('[nomarker]','')}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
              map = new google.maps.Map(mapElement, mapOptions);


              mr.maps.instances.push(map);
              jQuery(mapElement).trigger('mapCreated.maps.mr').get(0).dispatchEvent(mapCreatedEvent);
              map.setCenter(results[0].geometry.location);

              address.forEach(function(address){
                var markerGeoCoder;

                if(/(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)/.test(address) ){
                  var latlong = address.split(','),
                    marker = new google.maps.Marker(jQuery.extend({}, markerOptions, {
                      position: { lat: 1*latlong[0], lng: 1*latlong[1] },
                      map: map,
                    }));
                }
                else if(address.indexOf('[nomarker]') < 0){
                  markerGeoCoder = new google.maps.Geocoder();
                  markerGeoCoder.geocode( { 'address': address.replace('[nomarker]','')}, function(results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                      marker = new google.maps.Marker(jQuery.extend({}, markerOptions, {
                        map: map,
                        position: results[0].geometry.location,
                      }));
                    }
                    else{
                      console.log('Map marker error: '+status);
                    }
                  });
                }

              });
            } else {
              console.log('There was a problem geocoding the address.');
            }
          });
        }
        else if(typeof latitude !== typeof undefined && latitude !== "" && latitude !== false && typeof longitude !== typeof undefined && longitude !== "" && longitude !== false ){
          mapOptions.center   = { lat: latitude, lng: longitude};
          map                 = new google.maps.Map(mapElement, mapOptions);
          marker              = new google.maps.Marker(jQuery.extend({}, markerOptions, {
            position: { lat: latitude, lng: longitude },
            map: map }));
          mr.maps.instances.push(map);
          jQuery(mapElement).trigger('mapCreated.maps.mr').get(0).dispatchEvent(mapCreatedEvent);
        }
      });
    }
  }
}

function init_api(){
  if(document.querySelector('[data-maps-api-key]') && !document.querySelector('.gMapsAPI')){
    if($('[data-maps-api-key]').length){
      var script = document.createElement('script');
      var apiKey = $('[data-maps-api-key]:first').attr('data-maps-api-key');
      apiKey = typeof apiKey !== typeof undefined ? apiKey : '';
      if(apiKey !== ''){
        script.type = 'text/javascript';
        script.src = 'https://maps.googleapis.com/maps/api/js?key='+apiKey+'&callback=init_map';
        script.className = 'gMapsAPI';
        document.body.appendChild(script);
      }
    }
  }
}

// TODO Remove if not needed
function setBackgrounds() {
  $('.background-image-holder').each(function() {
    var imgSrc = $(this).children('img').attr('src');
    $(this).css('background', 'url("' + imgSrc + '")').css('background-position', 'initial').css('opacity','1');
  });
}
