timelabCookie.settings = {
  policy: {
    header: "Denna webbsida använder sig av Cookies",
    text: "<p>Denna hemsida använder sig utav cookies för att försäkra er om den bästa upplevelsen.</p>",
    link: {
      label: "Läs mer här",
      url: "javascript:enablePrivacy()",
    },
  },
  buttons: {
    toggler: {
      label: "Cookies",
      paddingTop: "5px",
      buttonHeight: "35px",
    },
    settings: {
      label: "Inställningar",
      class: "",
    },
    save: {
      label: "Spara medgivande",
      active: true,
      class: "tc-button--positive",
    },
    allow_all: {
      label: "Acceptera",
      active: true,
      class: "tc-button--positive",
    },
    deny_all: {
      label: "Neka alla",
      active: false,
      class: "",
    },
  },
  categories: {
    necessary: {
      label: "Nödvändiga",
      order: 0,
    },
    settings: {
      label: "Inställningar",
      order: 1,
    },
    statistics: {
      label: "Statistik",
      order: 2,
    },
    marketing: {
      label: "Marknadsföring",
      order: 3,
    }
  },
  styles: {
    maxWidth: "1180px",
    background: "#333333",
    color: "#fff",
    borderColor: "#101010",
    fontFamily: "Helvetica",
    buttons: {
      standard: {
        color: "#000",
        backgroundColor: "#f7f7f7"
      },
      positive: {
        color: "#fff",
        backgroundColor: "#8ed27d",
      }
    },
    foldOutCheckboxes: true,
  }
}