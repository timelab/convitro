$(document).ready(function () {
  console.log("In admin");

  function getBlockList() {
    return wp.data.select( 'core/editor' ).getBlocks();
  }

  var blockList = getBlockList();
  wp.data.subscribe(function() {
    var newBlockList = getBlockList();
    var blockListChanged = newBlockList !== blockList;
    blockList = newBlockList;
    if ( blockListChanged ) {
      window.setTimeout(init_all, 1000);
    }
  });
});