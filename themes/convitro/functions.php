<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php') ) . '</a></p></div>';
	});
	
	add_filter('template_include', function($template) {
		return get_stylesheet_directory() . '/static/no-timber.html';
	});
	
	return;
}

function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyCpuv1-lVaXqjwS9LHvZk-5Ez-j3tNwzDc');
}

add_action('acf/init', 'my_acf_init');

require_once 'lib/post-types.php';
require_once 'lib/custom-functions.php';
require_once 'lib/routes.php';

Timber::$dirname = array('templates', 'views');

class StarterSite extends TimberSite {

	function __construct() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		add_filter( 'block_categories', array( $this, 'convitro_block_category' ) , 1, 2);
		add_action( 'acf/init', array( $this, 'register_custom_blocks') );
		add_action( 'acf/init', array( $this, 'register_acf_options_pages') );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		add_action( 'init', array( $this, 'add_custom_thumbnail_sizes' ) );
		add_action( 'after_setup_theme', array($this, 'register_my_menu') );
		add_action( 'enqueue_block_editor_assets', array( $this, 'custom_gutenberg_assets' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_block_js_in_admin' ) );

		// Image sizes
		add_action( 'init', array($this, 'add_image_sizes') );

		parent::__construct();
	}

	function add_image_sizes() {
		add_image_size( 'video_thumbnail', 920, 458, array('center', 'center') );
		add_image_size( 'switchable_image', 865, 438, array('center', 'center') );
		add_image_size( 'team_image', 510, 346, array('center', 'center') );
		add_image_size( 'career_block_image', 552, 360, array('center', 'center') );
	}

	public function custom_gutenberg_assets() {
		// Load the theme styles within Gutenberg.
		wp_enqueue_style( 'custom-gutenberg', get_theme_file_uri( '/assets/css/backend.min.css' ), false );
  }

  // Add main.min.js to block editor
  function enqueue_block_js_in_admin() {
		$current_screen = get_current_screen();
    if ( method_exists( $current_screen, 'is_block_editor' ) && $current_screen->is_block_editor() ) {//Check if we're on a Gutenberg Page
    	wp_enqueue_script( 'convitro', get_template_directory_uri() . '/assets/js/blocks.js' );
    }
	}

	function convitro_block_category( $categories, $post ) {
		return array_merge(
			array(
				array(
					'slug' => 'convitro-blocks',
					'title' => __( 'Convitro', 'convitro-blocks' ),
				),
			),
			$categories
		);
	}

	function register_custom_blocks() {

		add_theme_support('align-wide');

		// Hero Section
		acf_register_block_type(array(
		  'name'              => 'hero-section',
		  'title'             => __('Hero Section'),
		  'description'       => __(''),
		  'render_template'   => 'custom-blocks/hero-section.php',
		  'category'          => 'convitro-blocks',
		  'icon'              => 'format-image',
		  'keywords'          => array( 'Hero', 'Section', 'content' ),
		  'align'							=> 'full',
		  'supports' 					=> array('align' => array('full'))
		));

		// Hero Section Split
		acf_register_block_type(array(
			'name'              => 'hero-section-split',
			'title'             => __('Hero Section Split'),
			'description'       => __(''),
			'render_template'   => 'custom-blocks/hero-section-split.php',
			'category'          => 'convitro-blocks',
			'icon'              => 'format-image',
			'keywords'          => array( 'Hero', 'Section', 'content', 'split' ),
			'align'							=> 'full',
			'supports' 					=> array('align' => array('full'))
		));

		// Featured Section
		acf_register_block_type(array(
		  'name'              => 'featured-section',
		  'title'             => __('Featured Section'),
		  'description'       => __(''),
		  'render_template'   => 'custom-blocks/featured-section.php',
		  'category'          => 'convitro-blocks',
		  'icon'              => 'editor-insertmore',
		  'keywords'          => array( 'Featured', 'Section', 'content' ),
		  'align'							=> 'full',
		  'supports' 					=> array('align' => array('full'))
		));

		// Video Section
		acf_register_block_type(array(
		  'name'              => 'video-section',
		  'title'             => __('Video Section'),
		  'description'       => __(''),
		  'render_template'   => 'custom-blocks/video-section.php',
		  'category'          => 'convitro-blocks',
		  'icon'              => 'video-alt3',
		  'keywords'          => array( 'Video', 'Section', 'content' ),
		  'align'							=> 'full',
		  'supports' 					=> array('align' => array('full'))
		));

		// Video Section
		acf_register_block_type(array(
		  'name'              => 'switchable-image-section',
		  'title'             => __('Switchable Image Section'),
		  'description'       => __(''),
		  'render_template'   => 'custom-blocks/switchable-image-section.php',
		  'category'          => 'convitro-blocks',
		  'icon'              => 'image-flip-horizontal',
		  'keywords'          => array( 'Switchable image', 'Section', 'content' ),
		  'align'							=> 'full',
		  'supports' 					=> array('align' => array('full'))
		));

		// Text section styled
		acf_register_block_type(array(
		  'name'              => 'text-section',
		  'title'             => __('Text Section (Styled)'),
		  'description'       => __(''),
		  'render_template'   => 'custom-blocks/text-section-styled.php',
		  'category'          => 'convitro-blocks',
		  'icon'              => 'format-aside',
		  'keywords'          => array( 'Text', 'Section', 'content' ),
		  'align'							=> 'full',
		  'supports' 					=> array('align' => array('full'))
		));

		// Tabbed view
		acf_register_block_type(array(
		  'name'              => 'tabbed-view-section',
		  'title'             => __('Tabbed view (Styled)'),
		  'description'       => __(''),
		  'render_template'   => 'custom-blocks/tabbed-view-section.php',
		  'category'          => 'convitro-blocks',
		  'icon'              => 'format-aside',
		  'keywords'          => array( 'Tab', 'Section', 'content' ),
		  'align'							=> 'full',
		  'supports' 					=> array('align' => array('full'))
		));

		// The Team Section
		acf_register_block_type(array(
		  'name'              => 'team-section',
		  'title'             => __('Team Section'),
		  'description'       => __(''),
		  'render_template'   => 'custom-blocks/team-section.php',
		  'category'          => 'convitro-blocks',
		  'icon'              => 'groups',
		  'keywords'          => array( 'Team', 'Section', 'content' ),
		  'align'							=> 'full',
		  'supports' 					=> array('align' => array('full'))
		));

		// White Paper Section
		acf_register_block_type(array(
		  'name'              => 'white-paper-section',
		  'title'             => __('White Paper Section'),
		  'description'       => __(''),
		  'render_template'   => 'custom-blocks/white-paper-section.php',
		  'category'          => 'convitro-blocks',
		  'icon'              => 'book-alt',
		  'keywords'          => array( 'White', 'Paper', 'Section' ),
		  'align'							=> 'full',
		  'supports' 					=> array('align' => array('full'))
		));

		// Career Block Section
		acf_register_block_type(array(
		  'name'              => 'career-block-section',
		  'title'             => __('Career Block Section'),
		  'description'       => __(''),
		  'render_template'   => 'custom-blocks/career-blocks-section.php',
		  'category'          => 'convitro-blocks',
		  'icon'              => 'welcome-learn-more',
		  'keywords'          => array( 'Career', 'Section' ),
		  'align'							=> 'full',
		  'supports' 					=> array('align' => array('full'))
		));

		// Contact Block Section
		acf_register_block_type(array(
		  'name'              => 'contact-block-section',
		  'title'             => __('Contact Block Section'),
		  'description'       => __(''),
		  'render_template'   => 'custom-blocks/contact-block-section.php',
		  'category'          => 'convitro-blocks',
		  'icon'              => 'location-alt',
		  'keywords'          => array( 'Contact', 'Section' ),
		  'align'							=> 'full',
		  'supports' 					=> array('align' => array('full'))
		));

		// Contact Banner Section
		acf_register_block_type(array(
			'name'              => 'contact-banner',
			'title'             => __('Contact Banner'),
			'description'       => __(''),
			'render_template'   => 'custom-blocks/contact-banner.php',
			'category'          => 'convitro-blocks',
			'icon'              => 'location-alt',
			'keywords'          => array( 'Contact', 'Section' ),
			'align'							=> 'full',
			'supports' 					=> array('align' => array('full'))
		));

		// Contact Banner Section
		acf_register_block_type(array(
			'name'              => 'text-section-5050',
			'title'             => __('50/50 Text Section'),
			'description'       => __(''),
			'render_template'   => 'custom-blocks/5050-text-section.php',
			'category'          => 'convitro-blocks',
			'icon'              => 'location-alt',
			'keywords'          => array( 'Text', 'Section' ),
			'align'							=> 'full',
			'supports' 					=> array('align' => array('full'))
		));


		// Contact Banner Section
		acf_register_block_type(array(
			'name'              => 'stadard-text-section',
			'title'             => __('Standard Text Section'),
			'description'       => __(''),
			'render_template'   => 'custom-blocks/standard-text-section.php',
			'category'          => 'convitro-blocks',
			'icon'              => 'location-alt',
			'keywords'          => array( 'Text', 'Section' ),
			'align'							=> 'full',
			'supports' 					=> array('align' => array('full'))
		));


		// Contact Banner Section
		acf_register_block_type(array(
			'name'              => 'reference-section',
			'title'             => __('Reference Section'),
			'description'       => __(''),
			'render_template'   => 'custom-blocks/reference-section.php',
			'category'          => 'convitro-blocks',
			'icon'              => 'location-alt',
			'keywords'          => array( 'Reference', 'Section' ),
			'align'							=> 'full',
			'supports' 					=> array('align' => array('full'))
		));

	}

	function register_acf_options_pages() {

    // Check function exists.
    if( !function_exists('acf_add_options_page') )
        return;

    // register options page.
    $option_page = acf_add_options_page(array(
        'page_title'    => __('Convitro General Settings'),
        'menu_title'    => __('Convitro Settings'),
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false,
        'position'			=> '20.1'
    ));
	}



	function register_post_types() {
		/**
		 * this is where you can register custom post types
		 * - - - - - - - - - - - - - - - - -
		 * Format for making a custom post type
		 * makeCustomPostType('singular', 'plural', 'icon', has single)
		 * -------------------------------------------------
		 * singluar = lowecase post type name in singular
		 * plural = same as singular but in plural form
		 * icon = code for icon can be src to an image or dashicon code
		 * Dash icons = https://developer.wordpress.org/resource/dashicons/
		 * has single = bool if the post type has single pages
	   */
	}

	function register_taxonomies() {
		//this is where you can register custom taxonomies
	}

	function register_my_menu() {
	  register_nav_menu( 'primary', __( 'Primary Menu', 'theme-slug' ) );
	}

	function add_to_context( $context ) {

		$context['menu'] = new TimberMenu();
		$context['site'] = $this;
		$context['social_icons'] = get_field('acf_social_icons', "option");
		$context['contact_bubble_link'] = get_field('acf_contact_bubble_link', "option");

		// Header
		$header_contact_button = get_field('acf_contact_button', 'option');
		$context['contact_button']['text'] = $header_contact_button['acf_contact_button_text'];
		$context['contact_button']['url'] = $header_contact_button['acf_contact_button_link'];

		global $post;
		$context['menu_class'] = '';
		$blocks  = parse_blocks($post->post_content);
		if($blocks && $blocks[0]['blockName'] == 'acf/hero-section') {
		  $context['menu_class'] = 'bar--absolute bar--transparent';
		}

		// Contact overlay
		$context['form']["title"] = get_field('acf_form_title', 'option');
		$context['form']["subtitle"] = get_field('acf_form_subtitle', 'option');
		$context['form']["id"] = get_field('acf_form_id', 'option');
		$context['form']["privacy_content"] = get_field('acf_privacy_content', 'option');


		// Footer
		$context['footer']['left_title'] = get_field('acf_footer_name_title_left', 'option');
		$context['footer']['address'] = get_field('acf_footer_address_group', 'option');
		$context['footer']['right_title'] = get_field('acf_footer_contact_title_right', 'option');
		$context['footer']['social'] = get_field('acf_footer_social', 'option');
		$context['footer']['fields'] = array();

		$footer_fields = get_field('acf_footer_contact_fields', 'option');
		foreach ($footer_fields as $field) {
			switch ($field['acf_fc_layout']) {
				case 'acf_footer_contact_fields_epost':
					// Email field
					array_push($context['footer']['fields'], array(
						'icon' => 'email',
						'text' => $field['epost'],
						'link' => 'mailto:'.$field['epost'],
						'target' => ''
					));
					break;
				case 'acf_footer_contact_fields_telefon':
					// Phone field
					array_push($context['footer']['fields'], array(
						'icon' => 'phone',
						'text' => $field['telefon'],
						'link' => 'tel:'.$field['telefon'],
						'target' => ''
					));
					break;
				case 'acf_footer_contact_fields_social_lank';
					// Social field
					array_push($context['footer']['fields'], array(
						'icon' => 'social',
						'text' => $field['lanktext'],
						'link' => $field['lank'],
						'target' => '_blank'
					));
					break;
				default:
					// Do nothing
					break;
			}
		}

		return $context;
	}

	function getContactArray(){
		if(get_field( 'acf_contact_location', 'options' )){
			$map = get_field( 'acf_contact_location', 'options' );
			$map['address_formated'] = $map['address'];

			$adressArr = explode(", ", $map['address']);

			$map['address'] = $adressArr[0];
			$map['zip'] = $adressArr[1];
			$map['city'] = $adressArr[2];
			$map['country'] = $adressArr[3];
			
			return $map;
		}else{
			return false;
		}
	}

	function myfoo( $text ) {
		$text .= ' bar!';
		return $text;
	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own functions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter('myfoo', new Twig_SimpleFilter('myfoo', array($this, 'myfoo')));
		return $twig;
	}

}

new StarterSite();

/* Custom Setting page */
function add_admin_menu_separator($position) {
    global $menu;
    $index = 0;
    if(is_array($menu)){
	    foreach($menu as $offset => $section) {
	        if (substr($section[2],0,9)=='separator')
	        $index++;
	        if ($offset>=$position) {
	            $menu[$position] = array('','read',"separator{$index}",'','wp-menu-separator');
	            break;
	        }
	    }
	    ksort( $menu );
  	}
}

add_action('admin_init','admin_menu_separator');
function admin_menu_separator() {add_admin_menu_separator('80.015');}