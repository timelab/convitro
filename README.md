# WP Template

## Usage

`docker compose up` to run

## Setup

* Update `.env` file
* Update `values.yaml`
* Update `values.prod.yaml`
* Add the following bitbucket repository variables to the repo:
    * `STAGING_NAMESPACE`
    * `PRODUCTION_NAMESPACE`

## Deploy

TODO

## Setup environment

### Prerequisites

* [Helm charts](https://bitbucket.org/timelab/helm-charts/src/master) cloned locally
* [wp-db-setup.sh](https://bitbucket.org/timelab/ops/src/master/wp/wp-db-setup.sh)


### Secrets

* Copy `secrets.yaml.tpl` to `secrets.yaml`
* Update `secrets.yaml` 
    * `db.username` - set to site name (or something else unique)
    * `db.password` - random generate password for database using `apg -M NCL -m 16 -n1`

### Staging

```
export WP_NAME=wp-template
export WP_NAMESPACE=$WP_NAME-staging
export WP_HELM_CHART=~/projects/helm-charts/wp

kubectl create namespace $WP_NAMESPACE
helm install -f values.yaml -f secrets.yaml $WP_NAME $WP_HELM_CHART --namespace $WP_NAMESPACE
```

After that, create DB and user using `wp-db-setup.sh` script found [here](https://bitbucket.org/timelab/ops/src/master/wp/wp-db-setup.sh)
(Namespace should be `WP_NAME-staging`)

### Production

```
export WP_NAME=insert-site-name-here
export WP_NAMESPACE=$WP_NAME-production
export WP_HELM_CHART=~/projects/helm-charts/wp

kubectl create namespace $WP_NAMESPACE
helm install -f values.yaml -f values.prod.yaml -f secrets.yaml $WP_NAME $WP_HELM_CHART --namespace $WP_NAMESPACE
```

After that, create DB and user using `wp-db-setup.sh` script found [here](https://bitbucket.org/timelab/ops/src/master/wp/wp-db-setup.sh)
(Namespace should be `WP_NAME-production`)
